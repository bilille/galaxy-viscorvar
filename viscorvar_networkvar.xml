<tool id="viscorvar_networkvar" name="visCorVar networkVar" version="@TOOL_VERSION@+galaxy0" profile="16.04" workflow_compatible="true">

    <description>creates a network between selected variables of datasets and the response variables. In the network, the similarity between two variables is associated with the link between these two variables.</description>

    <macros>
        <import>macros.xml</import>
        <import>macros_viscorvar.xml</import>
    </macros>

    <expand macro="requirements"/>
    <expand macro="stdio"/>

    <command detect_errors="aggressive"><![CDATA[

        @COMMAND_RSCRIPT@/viscorvar_networkvar.R

        --mat_similarity_rdata ${mat_similarity_rdata}
        --var_list_file ${var_list_file}
        --response_var ${select_response_var}

        --output_graph ${output_graph}

    ]]></command>

    <inputs>
        <param name="mat_similarity_rdata" type="data" format="rdata"
               label="this is the RData output file from the computeMatSimilarity function"
               help="output RData file from the computeMatSimilarity function"/>
        <param name="var_list_file" type="data" format="tabular"
               label="Variables list file"
               help="selected variables output tabular file from the circleCor function"/>

        <!-- Fichier avec noms de gènes/variables, donné par l'utilisateur -->
        <!-- <param name="var_of_interest_file" type="data" format="txt" optional="true"
               label="Variables of interest (Optional)"
               help="these variables have to belong to datasets that can be superimposed"/> -->

        <param name="response_var_list" type="data" format="tabular"
               label="List of response variables"
               help="output *_response_var.tsv file from the matCorAddVar function"/>
        <param name="select_response_var" type="select" display="checkboxes" multiple="true"
               label="Response variables"
               help="response variables that will be plotted in the network">
        <!-- <param name="select_responses_var" type="select" multiple="true" label="Response variables"> -->
            <options from_dataset="response_var_list">
                <column name="value" index="0"/>
                <filter type="unique_value" column="0"/>
                <filter type="sort_by" name="sorted_value" column="0"/>
            </options>
        </param>
    </inputs>

    <outputs>
        <data name="output_graph" format="xml" label="${tool.name}_graph.graphml" />
    </outputs>

    <tests>
    </tests>

    <help><![CDATA[

@HELP_AUTHORS@

====================
visCorVar networkVar
====================

-----------
Description
-----------

This tool creates a network of correlated variables for omics datasets. The determination of
the omics datasets which can be visualized is made by the matCorAddVar tool. This network can
be exported to graphml format and visualized with Cytoscape. The link between two variables
is associated with the correlation between two variables : a threshold can be used in
Cytoscape to get a network of variables whose correlations are greater than this threshold
in absolute value.

-----------------
Workflow position
-----------------

**Upstream tools**

================================ ============================================= =========
Name                             Output file                                   Format
================================ ============================================= =========
visCorVar.computeMatSimilarity   viscorvar_computematsimilarity_output.RData   rdata
-------------------------------- --------------------------------------------- ---------
visCorVar.circleCor              viscorvar_circlecor_selected_var.tsv          tabular
-------------------------------- --------------------------------------------- ---------
visCorVar.matCorAddVar           viscorvar_matcoraddvar_response_var.tsv       tabular
================================ ============================================= =========

---------------------------------------------------

-----------
Input files
-----------

+-----------------------------------------------------------+-----------+
| Parameter : num + label                                   |  Format   |
+===========================================================+===========+
| 1 : Rdata visCorVar.computeMatSimilarity output           |  rdata    |
+-----------------------------------------------------------+-----------+
| 2 : \*_selected_var.tsv visCorVar.circleCor output        |  tabular  |
+-----------------------------------------------------------+-----------+
| 3 : \*_response_var.tsv visCorVar.matCorAddVar output     |  tabular  |
+-----------------------------------------------------------+-----------+

----------
Parameters
----------

@HELP_MANUAL@

------------
Output files
------------

viscorvar_networkvar_graph.graphml
	GraphML file

    ]]></help>

    <expand macro="citations" />

</tool>
