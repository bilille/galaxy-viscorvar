#!/usr/bin/env Rscript

# Setup R error handling to go to stderr
options( show.error.messages=F, error = function () { cat( geterrmessage(), file=stderr() ); q( "no", 1, F ) } )

# we need that to not crash galaxy with an UTF8 error on German LC settings.
loc <- Sys.setlocale("LC_MESSAGES", "en_US.UTF-8")

## Get parameters ##
suppressPackageStartupMessages(require(argparse))

parser <- ArgumentParser(description='Compute the matCorEtBlockSelect and addVariablesReponses functions')

parser$add_argument('--input_rdata', dest='input_rdata', required=TRUE, help="Input RData file")
parser$add_argument('--cutoff_comp', dest='cutoff_comp', type='double', required=TRUE, help="")
parser$add_argument('--interest_var_file', dest='interest_var_file', required=FALSE, help="Variables of interest file")
parser$add_argument('--block_Y_file', dest='block_Y_file', required=TRUE, help="Block Y filepath")
parser$add_argument('--output_rdata', dest='output_rdata', required=TRUE, help="Output RData file")
parser$add_argument('--output_response_var', dest='output_response_var', required=TRUE, help="Output response variables file")
parser$add_argument('--output_blocks_comb', dest='output_blocks_comb', required=TRUE, help="Output blocks combinations file")

args <- parser$parse_args()

## Print parameters
print("Input RData:")
print(args$input_rdata)
print("Cutoff comp:")
print(args$cutoff_comp)
print("Variables of interest:")
print(args$interest_var_file)
print("Block Y file:")
print(args$block_Y_file)
print("Output RData:")
print(args$output_rdata)
print("Output Response variables:")
print(args$output_response_var)
print("Output Blocks combinations:")
print(args$output_blocks_comb)

## Loading libraries
suppressPackageStartupMessages(require(visCorVar))

# R script call
source_local <- function(fname)
{
    argv <- commandArgs(trailingOnly = FALSE)
    base_dir <- dirname(substring(argv[grep("--file=", argv)], 8))
    source(paste(base_dir, fname, sep="/"))
}

# Loading input Rdata file
load(args$input_rdata)

ncomp = mixomics_result$ncomp

# Reading Block Y matrix
print("Reading Block Y")
mat_block_Y = read.table(args$block_Y_file, header=TRUE, row.names=1)
print(mat_block_Y)

response_var = colnames(mat_block_Y)

print("response_var:")
print(response_var)

# Write response variables to output file
invisible(lapply(response_var, write, file=args$output_response_var, append=TRUE, ncolumns=1))

# Reading var of intereset file
interest_var_vec = NULL
if (args$interest_var_file != 'None') {
    interest_var_vec = as.character(read.table(args$interest_var_file, header=FALSE)[[1]])
}

#
comp = 1:2

# Running main function
res_matCorAddVar = matCorAddVar(res_block_splsda = mixomics_result,
                                block_Y = mat_block_Y,
                                cutoff_comp = args$cutoff_comp,
                                var_interest = interest_var_vec,
                                comp = comp)

#
mat_cor_comp1 = res_matCorAddVar$mat_cor_comp1
mat_cor_comp2 = res_matCorAddVar$mat_cor_comp2
list_vec_index_block_select  = res_matCorAddVar$list_vec_index_block_select
list_vec_names_blocks = res_matCorAddVar$list_vec_names_blocks
list_cor_comp_selected_var_resp_var = res_matCorAddVar$list_cor_comp_selected_var_resp_var
res_compute_cor_var_interest = res_matCorAddVar$res_compute_cor_var_interest

#
print("mat_cor_comp1:")
print(mat_cor_comp1)
print("mat_cor_comp2:")
print(mat_cor_comp2)
print("list_vec_index_block_select:")
print(list_vec_index_block_select)
print("list_vec_names_blocks:")
print(list_vec_names_blocks)
print("list_cor_comp_selected_var_resp_var:")
print(list_cor_comp_selected_var_resp_var)
print("res_compute_cor_var_interest:")
print(res_compute_cor_var_interest)

# Write all possible blocks combinations to output file
invisible(lapply(list_vec_names_blocks, write, file=args$output_blocks_comb, append=TRUE, ncolumns=100, sep=","))

# Save all objects in Rdata output file
save(res_matCorAddVar,
     file = args$output_rdata)
