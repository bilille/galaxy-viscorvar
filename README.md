# galaxy-viscorvar

Galaxy wrappers for the visCorVar R package and selected functions from the mixOmics R package (block.splsda, plotIndiv and plotVar).

## Install from Toolshed

Install `viscorvar` repository from Galaxy ToolShed (owner: `ppericard`)

## Install for developers

Clone repository

```
$ git clone --recurse-submodules https://gitlab.com/bilille/galaxy-viscorvar.git
```

Add `galaxy-viscorvar` repository to `$GALAXYDIR/tools` directory.

Edit `$GALAXYDIR/config/tool_conf.xml` and add:

```
<section id="development" name="Tools development">
  <label id="viscorvar" name="visCorVar" />
    <tool file="galaxy-viscorvar/mixomics_blocksplsda.xml" />
    <tool file="galaxy-viscorvar/mixomics_plotindiv.xml" />
    <tool file="galaxy-viscorvar/mixomics_plotvar.xml" />
    <tool file="galaxy-viscorvar/viscorvar_matcoraddvar.xml" />
    <tool file="galaxy-viscorvar/viscorvar_circlecor.xml" />
    <tool file="galaxy-viscorvar/viscorvar_computematsimilarity.xml" />
    <tool file="galaxy-viscorvar/viscorvar_networkvar.xml" />
</section>
```

