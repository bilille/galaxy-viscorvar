#' @title Determination of selected variables for all components
#' @description The function unionSelectBlockVariables determines, for each block, the selected block variables
#' for all components.
#' @param res_block_splsda type : sgccda. This parameter is the output of block.splsda function 
#' mixOmics.
#' @details For each block, the function unionSelectBlockVariables returns 1 if the block variable is selected for
#' at least one component. Otherwise, this function returns 0.
#' @return type : list of matrix. For each block, if the block variable is selected, the value 1 is associated with 
#' this block variable. Otherwise the value 0 is associated with this block variable. 
#' @examples 
#' data(res_data_integration)
#' list_union_selected_block_variables = unionSelectBlockVariables(res_data_integration)
#' @export
unionSelectBlockVariables <-function(res_block_splsda)
{
  ncomp = res_block_splsda$ncomp[1]
  
  names_blocks = names(res_block_splsda$loadings)
  index_Y = which(names_blocks == "Y")
  names_blocks = names_blocks[ - index_Y]
  list_select_block_variables = list()
  
  for(i in 1:length(names_blocks))
  {
    mat_loadings_i = res_block_splsda$loadings[[i]]
    index_i = c()
    
    for(j in 1:ncomp)
    {
      loadings_i_j = mat_loadings_i[, j]
      index_i_j = which(loadings_i_j != 0)
      
      index_i = c(index_i, index_i_j)
      
    } # End for(j 1:ncomp).
    
    index_i = unique(index_i)
    
    mat_select_block_variables = matrix(0,
                                        nrow = dim(mat_loadings_i)[1],
                                        ncol = 1)
    mat_select_block_variables[index_i, 1] = rep(1, length(index_i))
    rownames(mat_select_block_variables) = rownames(mat_loadings_i)
    
    list_select_block_variables[[i]] = mat_select_block_variables
    
  } # End for(i in 1:length(names_blocks)).
  
  names(list_select_block_variables) = names_blocks
  
  return(list_select_block_variables)
}
