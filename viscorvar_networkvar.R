#!/usr/bin/env Rscript

# Setup R error handling to go to stderr
options( show.error.messages=F, error = function () { cat( geterrmessage(), file=stderr() ); q( "no", 1, F ) } )

# we need that to not crash galaxy with an UTF8 error on German LC settings.
loc <- Sys.setlocale("LC_MESSAGES", "en_US.UTF-8")

## Get parameters ##
suppressPackageStartupMessages(require(argparse))

parser <- ArgumentParser(description='Run the networkVar function')

parser$add_argument('--mat_similarity_rdata', dest='mat_similarity_rdata', required=TRUE, help="matSimilarity RData file")
parser$add_argument('--var_list_file', dest='var_list_file', required=TRUE, help="Variables list file")
# parser$add_argument('--interest_var_file', dest='interest_var_file', required=FALSE, help="Variables of interest file")
parser$add_argument('--response_var', dest='response_var', required=TRUE, help="Response variables")
parser$add_argument('--output_graph', dest='output_graph', required=TRUE, help="Output graphml")

args <- parser$parse_args()

## Print parameters
print("matSimilarity RData file:")
print(args$mat_similarity_rdata)
print("Variables list file:")
print(args$var_list_file)
# print("Variables of interest:")
# print(args$interest_var_file)
print("Response variables:")
print(args$response_var)
print("Output graphml:")
print(args$output_graph)

## Loading libraries
# suppressPackageStartupMessages(require(mixOmics))
suppressPackageStartupMessages(require(igraph))

# R script call
source_local <- function(fname)
{
    argv <- commandArgs(trailingOnly = FALSE)
    base_dir <- dirname(substring(argv[grep("--file=", argv)], 8))
    source(paste(base_dir, fname, sep="/"))
}

## Loading local functions
suppressPackageStartupMessages(require(visCorVar))

# Loading input Rdata file
# loads res_compute_mat_similarity object
load(args$mat_similarity_rdata)

#
names_block_variables = as.character(read.table(args$var_list_file, header=FALSE)[[1]])
names_response_variables = strsplit(args$response_var, ",")[[1]]

# interest_var_vec = NULL
# if (args$interest_var_file != 'None') {
#     interest_var_vec = as.character(read.table(args$interest_var_file, header=FALSE)[[1]])
# }

print("names_block_variables:")
print(names_block_variables)
# print("interest_var_vec:")
# print(interest_var_vec)
print("names_response_variables:")
print(names_response_variables)

res_networkVar = networkVar(res_compute_mat = res_compute_mat_similarity,
                            type_cor = "similarity",
                            names_selected_variables = names_block_variables,
                            names_response_variables = names_response_variables,
                            cutoff = 0)

print("networkVar() completed")
print("res_networkVar:")
print(res_networkVar)

write.graph(res_networkVar$gR, file = args$output_graph, format = "graphml")
